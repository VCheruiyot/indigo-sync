#!/bin/env python

# This script fetchs legislation in Kenya Law's legacy XML format from
# indigo.kenyalaw.org, and pushes it into the kenyalaw.org staging website's
# ExistDB database.
#
# It is safe to run this regularly. The script stores the timestamp of the last
# time each work was pushed to ExistDB, and only re-publishes items that
# have changed. These timestamps are stored in timestamps.csv.

import os
import re
import csv
import logging
import datetime
import sys
from collections import defaultdict

import requests

skip_errors = False


# setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)8s %(message)s')
log = logging.getLogger(__name__)


TS_FNAME = "timestamps.csv"
TIMEOUT = 60 * 3
NOW = datetime.datetime.utcnow().isoformat()


# Indigo's API configuration
INDIGO_URL = 'https://kenya.staging.laws.africa/api/v2/akn'
INDIGO_AUTH_TOKEN = os.environ.get('INDIGO_AUTH_TOKEN')
if not INDIGO_AUTH_TOKEN:
    log.error("INDIGO_AUTH_TOKEN environment variable is not set.")
    sys.exit(1)


# exist-db configuration
EXISTDB_URL = os.environ.get('EXISTDB_URL', 'http://localhost:8080/exist/rest/')
EXISTDB_USER = os.environ.get('EXISTDB_USER')
EXISTDB_PASS = os.environ.get('EXISTDB_PASS')
EXISTDB_PATH = os.environ.get('EXISTDB_PATH', 'kenyalex/Kenya/Legislation/English')


# Setup connection to Indigo
indigo_session = requests.Session()
indigo_session.headers['Authorization'] = 'Token ' + INDIGO_AUTH_TOKEN


# Setup connection to exist-db
existdb_session = requests.Session()
if EXISTDB_USER:
    existdb_session.auth = (EXISTDB_USER, EXISTDB_PASS)


def subtype_year_number(frbr_uri):
    return re.match(r'/akn/ke/act/([^0-9][a-z-]*/)?(\d+)/([^/]+)', frbr_uri).groups()


def load_timestamps():
    """ Return a dict of frbr_uri to timestamps, describing when each work was last pulled from Indigo.
    """
    try:
        with open(TS_FNAME, "r") as f:
            reader = csv.DictReader(f)
            timestamps = {r['frbr_uri']: r['timestamp'] for r in reader}
            log.info("Loaded " + str(len(timestamps)) + " timestamps")
            return timestamps
    except IOError:
        log.warning("No " + TS_FNAME + ", assuming everything is new")

    return {}


def save_timestamps(timestamps):
    with open(TS_FNAME, "w") as f:
        writer = csv.DictWriter(f, fieldnames=['frbr_uri', 'timestamp'])
        writer.writeheader()

        for frbr_uri, timestamp in timestamps.items():
            writer.writerow({'frbr_uri': frbr_uri, 'timestamp': timestamp})

        log.info("Wrote timestamps to " + TS_FNAME)


def docs_to_sync(timestamps):
    """ Return JSON descriptions of the documents that need to be sync'd from Indigo.
    """
    items = []

    log.info("Loading items from Indigo at " + INDIGO_URL)

    # walk through everything published on indigo
    url = "/ke/.json"
    while url:
        resp = indigo_session.get(INDIGO_URL + url, timeout=TIMEOUT)
        resp.raise_for_status()
        data = resp.json()
        items.extend(data['results'])
        url = data['next']

    log.info("Got " + str(len(items)) + " works from Indigo")

    # ignore stubs and items that haven't changed
    items = [
        i for i in items
        if (i['updated_at'] > timestamps.get(i['frbr_uri'], '') and not i.get('stub'))
    ]

    log.info("Found " + str(len(items)) + " updated works")

    return items


def sync_from_indigo(timestamps, items):
    # map from parent frbr uri to list of children
    subleg = defaultdict(list)

    for info in items:
        try:
            if info.get('parent_work'):
                # subleg
                subleg[info['parent_work']['frbr_uri']].append(info)

            else:
                # primary work
                sync_work_from_indigo(info)
                timestamps[info['frbr_uri']] = NOW
        except requests.exceptions.RequestException as e:
            if skip_errors:
                log.error('Error for ' + info['frbr_uri'], exc_info=e)
            else:
                raise

    # sync subleg
    for frbr_uri, children in subleg.items():
        parent = children[0]['parent_work']
        try:
            sync_subleg_from_indigo(children, parent)

            for child in children:
                timestamps[child['frbr_uri']] = NOW
        except requests.exceptions.RequestException as e:
            if skip_errors:
                log.error('Error for subleg for ' + parent['frbr_uri'], exc_info=e)
            else:
                raise


def sync_work_from_indigo(info):
    """ Fetch this work from Indigo, including images, and store them in ExistDB.
    """
    log.info("Syncing " + info['frbr_uri'])

    path = path_prefix_for_work(info)
    fname = filename_for_work(info)

    # legacyxml content
    resp = indigo_session.get(info['url'] + '.legacyxml', timeout=TIMEOUT)
    resp.raise_for_status()
    # this is a binary string
    xml = resp.content
    xml = process_xml(xml)
    save_to_exist(path + '/' + fname + '.xml', 'application/xml', xml)

    # pdf
    resp = indigo_session.get(info['url'] + '.pdf', timeout=TIMEOUT)
    resp.raise_for_status()
    # this is a binary string
    pdf = resp.content
    save_to_exist(path + '/docs/' + fname + '.pdf', 'application/pdf', pdf)

    # fetch images
    images = get_images(info)
    if images:
        sync_images(info['frbr_uri'], path, images)

    log.info("Finished syncing " + info['frbr_uri'])


def sync_images(frbr_uri, path, images):
    log.info("Syncing " + str(len(images)) + " images for " + frbr_uri)

    for img in images:
        log.info("Getting image " + img['filename'])

        resp = indigo_session.get(img['url'], timeout=TIMEOUT)
        resp.raise_for_status()
        # this is a binary string
        data = resp.content
        save_to_exist(path + '/images/' + img['filename'], img['mime_type'], data)


def sync_subleg_from_indigo(children, parent):
    """ Find and sync subleg for a parent work. The parent is only the frbr_uri and the title.
    """
    child = children[0]
    log.info("Syncing subleg for " + parent['frbr_uri'] + " using child work " + child['frbr_uri'])

    path = path_prefix_for_work(parent) + "/subsidiary_legislation"
    fname = filename_for_work(parent)

    # legacyxml content for all subleg, from arbitrary child
    resp = indigo_session.get(child['url'] + '.legacyxmlsubleg', timeout=TIMEOUT)
    resp.raise_for_status()
    # this is a binary string
    xml = resp.content
    xml = process_xml(xml)
    save_to_exist(path + '/' + fname + '_subsidiary.xml', 'application/xml', xml)

    # pdf
    resp = indigo_session.get(child['url'] + '.pdf', timeout=TIMEOUT)
    resp.raise_for_status()
    # this is a binary string
    pdf = resp.content
    save_to_exist(path + '/docs/' + fname + '_subsidiary.pdf', 'application/pdf', pdf)

    # fetch images
    log.info("Syncing images for " + str(len(children)) + " subsidiary items")
    for info in children:
        images = get_images(info)
        if images:
            log.info("Syncing " + str(len(images)) + " images for " + info['frbr_uri'])
            sync_images(info['frbr_uri'], path, images)

    log.info("Finished syncing subleg for " + parent['frbr_uri'])


def path_prefix_for_work(info):
    """ Build up a path based on the work name.

    /db/kenyalex/Kenya/Legislation/English/Acts and Regulations/A/Advocates Act Cap. 16 - No. 18 of 1989
    """
    path = '/Acts and Regulations/' + info['title'][0].upper() + '/'

    title = info['title']
    if not title.endswith(' Act'):
        title = title + ' Act'

    path = path + title

    # we may only have the title and frbr_uri
    cap = None
    if 'url' not in info:
        # we only have the title and frbr_uri, guess the cap
        # number from the title
        match = re.search(r'\bCap\. (\d+)\b', info['title'])
        if match:
            cap = match.group(1)
    else:
        props = {p['key']: p['value'] for p in info.get('custom_properties', [])}
        cap = props.get('cap_number')

    if cap:
        path = path + ' Cap. ' + cap

    # get subtype and number from frbr_uri
    subtype, year, number = subtype_year_number(info['frbr_uri'])
    if not subtype:
        path = path + " - No. " + number + " of " + year

    return EXISTDB_PATH + path


def filename_for_work(info):
    """ XML filename for a work.
    eg. AdvocatesAct18of1989
    """
    fname = info['title'].replace(' ', '')
    if not fname.endswith('Act'):
        fname = fname + 'Act'

    subtype, year, number = subtype_year_number(info['frbr_uri'])
    if not subtype:
        fname = fname + number + 'of' + year

    return fname


def get_images(info):
    """ Get a list of image attachment descriptions for all points in time of this work.
    """
    # images, dict of image filename to info dict
    all_images = {}

    # urls for all points in time
    urls = [e['url'] for pit in info['points_in_time'] for e in pit['expressions']]

    for url in urls:
        # paginate for each expression URL
        url = url + '/media.json'

        while url:
            resp = indigo_session.get(url, timeout=TIMEOUT)
            resp.raise_for_status()
            data = resp.json()

            # only keep image attachments
            images = [x for x in data['results'] if x['mime_type'].startswith('image/')]

            for img in images:
                # check duplicates
                if img['filename'] in all_images:
                    log.warning("Different points in time of " + info['frbr_uri'] + " both have an image named " + img['filename'] + "."
                                " All images should have unique names, otherwise they will clash.")

                all_images[img['filename']] = img

            # pagination
            url = data['next']

    return all_images.values()


def save_to_exist(path, content_type, data):
    headers = {
        'Content-Type': content_type,
        'Content-Length': str(len(data)),
    }
    url = EXISTDB_URL + path
    log.info("Writing " + content_type + " to exist-db: " + url)
    resp = existdb_session.put(url, data=data, headers=headers)
    resp.raise_for_status()


def process_xml(xml):
    # adjust image sources
    return re.sub(r'\bsrc="media/', 'src="', xml)


def main():
    timestamps = load_timestamps()
    to_sync = docs_to_sync(timestamps)
    sync_from_indigo(timestamps, to_sync)
    save_timestamps(timestamps)
    log.info("Done, bye.")


if __name__ == '__main__':
    skip_errors = '--skip-errors' in sys.argv
    main()
