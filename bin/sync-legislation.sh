#!/bin/bash
set -eo pipefail

# load config
. syncer.env

# run script, skipping errors
python bin/sync-legislation.py --skip-errors 2>&1 | tee -a sync-legislation.log
