# indigo-syncer

Periodically pulls XML from indigo.kenyalaw.org and pushes it into existdb.

## Installation

Requires Python 2.6!

1. Clone this repo and change into the directory
2. Install dependencies: ``pip install -r requirements.txt``

## Configuration

The following environment variables must be set to configure the syncer.

* ``INDIGO_AUTH_TOKEN``: authentication token for Indigo, taken from [https://kenya.staging.laws.africa/accounts/profile/api/](https://kenya.staging.laws.africa/accounts/profile/api/)
* ``EXISTDB_URL``: URL to the exist-db REST API, such as ``http://localhost:8080/exist/rest/``
* ``EXISTDB_USER``: exist-db username (optional)
* ``EXISTDB_PASS``: exist-db password (optional)
* ``EXISTDB_PATH``: root path in the exist-db database where files must be stored, eg `kenyalex/Kenya/Legislation/English`


Create a file named ``syncer.env`` in the same directory as this README.md:

```bash
export INDIGO_AUTH_TOKEN=your-auth-token
export EXISTDB_URL=http://localhost:8080/exist/rest
export EXISTDB_USER=admin
export EXISTDB_PASS=
export EXISTDB_PATH=/kenyalaw/acts
```

## Running

To run this script, in the folder where this README.md is, run:

``bin/sync-legislation.sh``
